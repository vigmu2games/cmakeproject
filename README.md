Basic Template for game projects based on CMake and C/C++.

Project starts with: 
    CONFIGURATION:
    source folder (SOURCE) - folder named w/e you want
        CMakeLists.txt inside has info for how project will be
	    built during generating step.	
	GENERATION:
	build folder holds all executables and libraries that 
        CMakeLists.txt specifies (BINARY)
	    CMakeCache.txt inside
	
CMake has two steps: configuration and generation
    configuration define targets (executable, library, 
	and other outputs for the build pipeline).
	
Using the CMake-GUI rather than command line